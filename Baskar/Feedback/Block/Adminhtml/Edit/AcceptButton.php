<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Block\Adminhtml\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class AcceptButton
 * @package Baskar\Feedback\Block\Adminhtml\Edit
 */
class AcceptButton extends Generic implements ButtonProviderInterface
{
    /**
     * Retrieve button-specified settings
     * checks feedback status of the customer
     * @return array depending on status
     */
    public function getButtonData()
    {
        $status = $this->getModel()->load($this->id)->getStatus();
        $data = [];
        if ($status != 'Accepted') {
            $data = [
                'label' => __('Save'),
                'class' => 'save primary',
                'on_click' => sprintf('location.href="%s"', $this->getBackUrl()),
                'sort_order' => 10
            ];
        }
        return $data;
    }

   /**
     * To navigate to the  save controller file
     * @return string URL
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/save');
    }

}
