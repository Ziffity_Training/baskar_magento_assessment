<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Block\Adminhtml\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class DeclineButton
 * @package Baskar\Feedback\Block\Adminhtml\Edit
 */
class DeclineButton extends Generic implements ButtonProviderInterface
{
     /**
     * Retrieve button-specified settings
     * Reject or Decline button
     * @return array
     */
    public function getButtonData()
    {
        $status = $this->getModel()->load($this->id)->getStatus();
        $decline = [];
        if ($status != 'Declined') {
            $decline = [
                'label' => __('Reject'),
                'class' => 'save primary',
                'on_click' => sprintf('location.href="%s"', $this->getBackUrl()),
                'sort_order' => 10
            ];
        }
        return $decline;
    }
    /**
     * return url path when Reject button clicks
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/Decline');
    }
}
