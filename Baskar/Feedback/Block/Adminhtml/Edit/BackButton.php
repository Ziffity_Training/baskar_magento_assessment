<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Block\Adminhtml\Edit;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class BackButton
 * @package Baskar\Feedback\Block\Adminhtml\Edit
 */
class BackButton extends Generic implements ButtonProviderInterface
{
     /**
     * Retrieve button-specified settings
     * @return array
     */
    public function getButtonData()
    {
        return [
            'label' => __('Back'),
            'class' => 'back',
            'on_click' => sprintf('location.href="%s"', $this->getBackUrl()),
            'sort_order' => 10
        ];
    }
     /**
     * set url path when Return button clicks
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*');
    }

}
