<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Block\Adminhtml\Edit;

use Magento\Backend\Block\Widget\Context;
use Baskar\Feedback\Model\AddFeedbackFactory;

/**
 * Class Generic
 * class used for other buttons to extends its functions
 * @package Baskar\Feedback\Block\Adminhtml\Edit
 */
class Generic
{
    /**
     * @var id
     */
    protected $id;
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;
    /**
     * @var AddFeedbackFactory
     */
    protected $addFeedbackFactory;
   
    /**
     * Generic constructor.
     * @param  $context
     * @param  $addFeedback
     */
    public function __construct(Context $context, AddFeedbackFactory $addFeedback)
    {

        $this->id = $context->getRequest()->getParam('id');
        $this->urlBuilder = $context->getUrlBuilder();
        $this->addFeedbackFactory = $addFeedback;
    }
    public function getModel()
    {
        return $this->addFeedbackFactory->create();
    }
      /**
     * create url and set id to it
     * @param $route
     * @return string
     */
    public function getUrl($route)
    {
        return $this->urlBuilder->getUrl($route, ['id' => $this->id]);
    }

}
