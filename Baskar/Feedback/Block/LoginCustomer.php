<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Block;

use Magento\Framework\View\Element\Template\Context;
use Magento\Customer\Model\SessionFactory;

/**
 * Class LoginCustomer
 * @package Baskar\Feedback\Block
 */
class LoginCustomer extends \Magento\Framework\View\Element\Template
{
    /**
     * @var SessionFactory $_customerSession
     */
    protected $_customerSession;
   
    /**
     * LoginCustomer constructor.
     * @param  $context
     * @param  $customerSession
     * @param array $data
     */
    public function __construct(
            Context $context, 
            SessionFactory $customerSession,
            array $data = []
    )
    {
        $this->_isScopePrivate = true;
        $this->_customerSession = $customerSession->create();
        ;
        parent::__construct($context, $data);
    }
    /**
     * get Id of the logged in customer
     * @return \Magento\Customer\Model\Session
     */
    public function getCustomerSession()
    {
        return $this->_customerSession;
    }
     /**
     * get First Name of the logged in customer
     * @return bool|string
     */
    public function getUserFirstName()
    {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getCustomerData()->getFirstname();
        }
        return false;
    }
      /**
     * get Last Name of the logged in customer
     * @return bool|string
     */
    public function getUserLastName()
    {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getCustomerData()->getLastname();
        }
        return false;
    }
    /**
     * get Email of the logged in customer
     * @return bool|string
     */
    public function getUserEmail()
    {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getCustomerData()->getEmail();
        }
        return false;
    }
}
