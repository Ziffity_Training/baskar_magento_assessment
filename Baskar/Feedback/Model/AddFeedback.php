<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Model;

/**
 * Class AddFeedback
 * @package Baskar\Feedback\Model
 */
class AddFeedback extends \Magento\Framework\Model\AbstractModel
{
    public function _construct()
    {
        $this->_init("Baskar\Feedback\Model\ResourceModel\AddFeedback");
    }

}
