<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;

/**
 * Class AddFeedback
 * @package Baskar\Feedback\Model\ResourceModel
 */
class AddFeedback extends AbstractDb
{

    /**
     * Data constructor.
     * @param Context $context
     */
    public function __construct(
        Context $context
    ) {
        parent::__construct($context);
    }
    /**
     * Resource initialization
     *
     * @return void
     */
    protected function _construct()
    {
       $this->_init('customers_feedback', 'customer_id');
    }

}
