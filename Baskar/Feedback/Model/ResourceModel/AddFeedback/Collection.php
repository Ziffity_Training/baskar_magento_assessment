<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Model\ResourceModel\AddFeedback;

use Baskar\Feedback\Model\ResourceModel\AddFeedback as AddFeedbackRS;
use Baskar\Feedback\Model\AddFeedback;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package Baskar\Feedback\Model\ResourceModel\AddFeedback
 */
class Collection extends AbstractCollection
{
    
    protected $_idFieldName = 'customer_id';
    
    
    public function _construct()
    {
        $this->_init(AddFeedback::class, AddFeedbackRS::class);
    }
}
