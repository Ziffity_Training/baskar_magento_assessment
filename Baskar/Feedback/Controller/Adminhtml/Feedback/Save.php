<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Controller\Adminhtml\Feedback;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\RedirectFactory;
use Baskar\Feedback\Model\AddFeedback;
use Baskar\Feedback\Helper\FeedbackMail;

/**
 * Class Save
 * @package Baskar\Feedback\Controller\Adminhtml\Feedback
 */
class Save extends Action
{
    /**
     * @var AddFeedback
     */
    protected $addFeedback;
    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;
    /**
     * @var FeedbackMail
     */
    protected $mail;
    /**
     * @param FeedbackMail $mail
     * @param RedirectFactory $redirectFactory
     * @param Action\Context $context
     * @param AddFeedback $addFeedback
     */
    public function __construct(
            FeedbackMail $mail,
            RedirectFactory $redirectFactory,
            Action\Context $context,
            AddFeedback $addFeedback)
    {
        $this->addFeedback = $addFeedback;
        $this->resultRedirectFactory = $redirectFactory;
        $this->mail = $mail;
        parent::__construct($context);
    }

    /**
     * checks Whether user has access in acl
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Baskar_Feedback::feedback');
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Redirect|\Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam("id");
        if ($id) {
            $model = $this->addFeedback;
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__("This Member does not exist"));
            } else {
                $result = $this->resultRedirectFactory->create();
                try
                {
                    $this->mail->sendFeedbackMail($model->getData('user_email'), $model->getData('first_name'), $model->getData('feedback'), 'Your Feedback has been accepted'); 
                    $this->messageManager->addSuccessMessage(__('Feedback Accepted '));
                    $model->setStatus('Accepted');
                    $model->save();
                    
                } catch (\Exception $ex) {
                      $this->messageManager->addErrorMessage(__("Error in sending mail"));
                      
                }
                return $result->setPath('*/*');
            }
        }
    }

}
