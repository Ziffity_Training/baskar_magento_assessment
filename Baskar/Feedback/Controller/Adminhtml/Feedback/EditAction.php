<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Controller\Adminhtml\Feedback;

use Magento\Backend\App\Action;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Baskar\Feedback\Model\AddFeedback;

/**
 * Class EditAction
 * @package Baskar\Feedback\Controller\Adminhtml\Feedback
 */
class EditAction extends Action
{
    /**
     * @var PageFactory
     */
    protected $pageFactory;
    /**
     * @var AddFeedback
     */
    protected $addFeedback;
    /**
     * @param Action\Context $context
     * @param PageFactory $pageFactory
     * @param AddFeedback $addFeedback
     */
    public function __construct(Action\Context $context, PageFactory $pageFactory, AddFeedback $addFeedback)
    {
        $this->addFeedback = $addFeedback;
        $this->pageFactory = $pageFactory;
        parent::__construct($context);
    }
   /**
     * checks Whether user has access in acl
     * if not redirects to homepage
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Baskar_Feedback::feedback');
    }
     /**
     * Execute action based on request and return result
     * @return \Magento\Framework\Controller\ResultInterface|ResponseInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam("id");
        if ($id) {
            $model = $this->addFeedback;
            $model->load($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__("This Member does not exist"));
                $result = $this->resultRedirectFactory->create();
                return $result->setPath('feedback/feedback/index');
            }
        }
        $result = $this->pageFactory->create();
        $result->getConfig()->getTitle()->set('Feedback');
        return $result;
    }

}
