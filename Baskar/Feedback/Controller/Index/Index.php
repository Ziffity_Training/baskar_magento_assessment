<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Backend\Model\View\Result\RedirectFactory;

/**
 * Class Index
 * @package Baskar\Feedback\Controller\Index
 */
class Index extends Action
{

    /**
     * @var PageFactory
     */
    protected $pageFactory;
    
     /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;
   
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;
    /**
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(Context $context,
            PageFactory $pageFactory,ScopeConfigInterface $scopeConfig,
            RedirectFactory $redirectFactory)
    {
        $this->_scopeConfig=$scopeConfig;
        $this->pageFactory = $pageFactory;
        $this->resultRedirectFactory = $redirectFactory;
        parent::__construct($context);
    }
     /**
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        
         $storeAdmin = $this->_scopeConfig->getValue('first_section/feedback_group/option', ScopeInterface::SCOPE_STORE);
         if($storeAdmin)
         {
            $result = $this->pageFactory->create();
            $result->getConfig()->getTitle()->set('Feedback form');
            return $result; 
         }else
         {
             $result = $this->resultRedirectFactory->create();  
             return $result->setPath('/');
         } 
            
       
    }

}
