<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Baskar\Feedback\Model\AddFeedbackFactory;
use Baskar\Feedback\Helper\FeedbackMail;

/**
 * Class FeedbackSave
 * @package Baskar\Feedback\Controller\Index
 */
class FeedbackSave extends Action
{
    /**
     * @var AddFeedbackFactory
     */
    protected $feedback;

    /**
     * @var Mail
     */
    protected $mail;

    /**
     * @param Mail $mail
     * @param Context $context
     * @param AddFeedbackFactory $feedback

     */
    public function __construct(
            FeedbackMail $mail,
            Context $context, 
            AddFeedbackFactory $feedback
          )
    {
        $this->feedback = $feedback;
        $this->mail = $mail;
        parent::__construct($context);
    }
      /**
     * @return ResponseInterface|\Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $post = (array) $this->getRequest()->getPost();
        $result = $this->resultRedirectFactory->create();
        
        if (!empty($post['firstname'] && !empty($post['lastname'] && !empty($post['useremail'] && !empty($post['feedback']))))) {
            $firstName = $post['firstname'];
            $lastName = $post['lastname'];
            $userEmail = $post['useremail'];
            $feedback = $post['feedback'];
            $block=\Magento\Framework\App\ObjectManager::getInstance()->get('Baskar\Feedback\Block\LoginCustomer');
           $firstName1= $block->getUserFirstName();
           $lastName1=$block->getUserLastName();
           $userEmail1= $block->getUserEmail();
           $guest=0;
           if($userEmail1==false)
           {
               $guest=1;
           }
           if($guest==0 &&( $firstName!=$firstName1 || $lastName!=$lastName1 || $userEmail!=$userEmail1))
           {
                $this->messageManager->addErrorMessage('You tried to put proxy');
            $result->setPath('*/*/index');
            return $result;
           }
            $model = $this->feedback->create();
            $model->addData([
                'first_name' => $firstName,
                'last_name' => $lastName,
                'user_email' => $userEmail,
                'feedback' => $feedback
            ]);  
              try
                {
                   $this->mail->sendFeedbackMail($model->getData('user_email'), $model->getData('first_name'), $model->getData('feedback'), "Thanks for the feedback !!!"); 
                   $saveData = $model->save();
                   if (!$saveData) 
                       {
                            $this->messageManager->addErrorMessage('Feedback not saved !');
                            $result->setPath('*/*/index');
                            return $result;
                       }
                    $this->messageManager->addSuccessMessage('hi '.$firstName.' '.$lastName.' Thank You For your valuable feedback !');
                    $result->setPath('');
                } catch (\Exception $ex) 
                {
                          $this->messageManager->addErrorMessage('Error in sending mail');
                          $result->setPath('*/*/index');
                                            
                            
                }    
                return $result;
            }
      
    }

}   
