<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Controller\FormPage;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\JsonFactory;
use Baskar\Feedback\Model\AddFeedback;
use Magento\Store\Model\ScopeInterface;

/**
 * Class ApprvedFeedback
 * @package Baskar\Feedback\Controller\FormPage
 */
class ApprovedFeedback extends Action
{

    /** @var JsonFactory */
    protected $jsonFactory;

    /** @var Feedback */
    protected $feedbackModel;

    /** @var ScopeConfigInterface */
    protected $scopeConfig;

    /**
     * @param Context $context
     * @param JsonFactory $jsonFactory
     * @param Feedback $feedbackModel
     * @param ScopeConfigInterface $scopeConfig
     */

    public function __construct(Context $context, JsonFactory $jsonFactory, AddFeedback $feedbackModel, ScopeConfigInterface $scopeConfig)
    {
        $this->jsonFactory = $jsonFactory;
        $this->feedbackModel = $feedbackModel;
        $this->scopeConfig = $scopeConfig;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface
     */
    public function execute()
    {
          $storeAdmin = $this->scopeConfig->getValue('first_section/feedback_group/option', ScopeInterface::SCOPE_STORE);
          if($storeAdmin)
          {
            $collection = $this->feedbackModel->getCollection();
            $collection->addFieldToSelect(array('first_name', 'last_name', 'user_email'))->addFieldToFilter('status', ['eq' => 'Accepted']);

            $result = $this->jsonFactory->create();
            $response['feedback_status'] = false;

            $response['data'] = $collection->getData();

            if(!empty($response['data'])) {
                $response['feedback_status'] = true;
            }

            $result->setData($response);
            return $result;
          }
    }

}
