<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Baskar\Feedback\Helper;

use Magento\Store\Model\Store;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Mail\Template\TransportBuilder;
use Magento\Framework\Translate\Inline\StateInterface;
use Magento\Backend\App\Action;
use Magento\Framework\Message\ManagerInterface; 

/**
 * Class Mail
 * @package Baskar\Feedback\Helper
 */
class FeedbackMail 
{
    /**
     * @var ManagerInterface
     */
    protected $messageManager;
    
    /**
     * @var StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var TransportBuilder
     */
    protected $transportBuilder;
    /**
     *
     * @var ScopeConfigInterface 
     */
    protected $scopeConfig;

    /**
     * @param ScopeInterface $scopeConfig
     * @param TransportBuilder $transportBuilder
     * @param StateInterface $inlineTranslation
     */
    public function __construct(ScopeConfigInterface $scopeConfig,
            TransportBuilder $transportBuilder,
            StateInterface $inlineTranslation,
            ManagerInterface $messageManager
            )
    {
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->messageManager=$messageManager;
    }

    /**
     * sends feedback reply mail
     * @param type $userEmail
     * @param type $firstName
     * @param type $feedback
     * @param type $message
     * @return void
     */ 
    public function sendFeedbackMail($userEmail, $firstName, $feedback, $message)
    {
         $this->inlineTranslation->suspend();      
        
        try
        {
             $admin=$this->scopeConfig->getValue('trans_email/ident_general/email', ScopeInterface::SCOPE_STORE);
             $transport = $this->transportBuilder->setTemplateIdentifier('mail_template')
                ->setTemplateOptions(['area'  => \Magento\Framework\App\Area::AREA_FRONTEND, 'store' => Store::DEFAULT_STORE_ID])
                ->setTemplateVars([ 
                                      'name'  =>  $firstName,
                                    'feedback'=> $feedback,
                                    'message' => $message])
                ->setFrom(['email' => $admin, 'name' => 'noreply@owner.com'])
                ->addTo([$userEmail])
                ->addBcc($admin)
                ->getTransport();
        $transport->sendMessage();
        } finally
        {
              $this->inlineTranslation->resume();
        }

    }
}
