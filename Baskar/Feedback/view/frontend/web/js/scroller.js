/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define(
    ['jquery', 'carousel'],
    function ($) {
        $.get('feedback/formpage/approvedfeedback', function (resp) {
            let htmlPage = "";
            if (resp.feedback_status) {
                resp.data.forEach(function (item) {

                    htmlPage += "<div class='item'>" +
                        "<div><b>First name :</b> " + item.first_name + "</div>" +
                        "<div><b>Last name :</b> " + item.last_name + "</div>" +
                        "<div><b>Email ID :</b> " + item.user_email + "</div>" +
                        "</div>"
                });
                $('#myCarousel').html(htmlPage);

                'use strict';
                $('.owl-carousel').owlCarousel({
                    loop: true,
                    margin: 10,
                    items: 1,
                    nav: true,
                });
            }

        });
    }
);
