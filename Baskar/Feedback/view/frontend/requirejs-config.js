/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

var config = {
    map: {
        '*': {
            scroller: 'Baskar_Feedback/js/scroller',
            carousel: 'Baskar_Feedback/js/carousel/owl.carousel.min',
        }
    }
};